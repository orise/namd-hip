# 东方超算系统NAMD使用指南

文档版本v1.0， 2023年4月2日，徐顺，xushun@sccas.cn



## 1. 简介

NAMD分子动力学模拟软件由University of Illinois at Urbana- Champaign的Beckman Institute for Advanced Science and Technology的Theoretical and Computational Biophysics Group课题组研发。该软件荣获2002年Gordon Bell奖和2012年Sidney Fernbach奖。基于Charm++并行对象可实现十万核以上大规模模拟，实现了30万核模拟1亿原子的体系。NAMD软件官方网站提供丰富的用户文档，见http://www.ks.uiuc.edu/Research/vmd/current/docs.html 。

NAMD2是对NAMD1重写，将PVM并改为Charm++并行方式。NAMD2 使用CHARMm力场和X-PLOR坐标和分子结构文件 ，当前版本支持四种力场：CHAEMM，X-PLOR，AMBER和Gromacs。NAMD 2.7增加了GPU，TIP4P和Collective Variable计算。NAMD 2.15支持ROCm平台编译。NAMD 3.0 [alpha版本](https://www.ks.uiuc.edu/Research/namd/alpha/3.0alpha/)开始支持单节点（single node）版本，主要面向单节点[多GPU卡](https://developer.nvidia.com/blog/delivering-up-to-9x-throughput-with-namd-v3-and-a100-gpu/)计算场景。

东方超算系统的类GPU计算硬件卡称为DCU，对应的软件运行和开发环境为DTK；DTK/DCU兼容AMD的ROCm/HIP GPU环境。NAMD-HIP版本可以直接在东方超算DTK/DCU环境下编译和运行。

使用HIP版本的NAMD，**首先推荐阅读NAMD HIP版本说明** https://www.ks.uiuc.edu/Research/namd/alpha/2.15_amdhip 。

有几点**注意事项**

1. 官方已提供NAMD的HIP [二进制版本](https://www.ks.uiuc.edu/Research/namd/alpha/2.15_amdhip/download/NAMD_2.15a1_Linux-x86_64-multicore-AMDHIP.tgz)（解压文件包之后可运行），但它是基于ROCm 3.9.0环境编译的，因此使用时需要加载ROCm 3.9.0环境；更多的ROCm版本需要自己编译。
2. NAMD的AMD HIP/ROCm版本与NAMD 2.14的CUDA版本具有相同的功能集，因为它们在很大程度上使用相同的代码路径。这意味着replica和non-replica 平衡模拟都能很好地工作，并且像COLVARS这样的功能是可用的。系统大小没有固有的限制。然而，并不是所有的模拟都适合这些AMD HIP编译版本，因为炼金术模拟，局部增强采样，表格能量，成对相互作用，压力分布计算和QM/MM不支持。
3. 由于最小化算法在NAMD中主要是基于CPU的实现方式与NAMD将分子系统空间分解为patch的并行方式，最小化和动力学过程应该在NAMD两次单独的run中执行。这确保了NAMD将以最优方式重新计算动力学的patch分解，从而为模拟提供最佳的整体性能。
4. NAMD ROCm平台版本代码仍在开发中，测试编译将根据需要进行更新，需持续关注!

## 2. 编译安装

以下介绍基于源代码的DTK（ROCm/HIP的国产适配版）多节点安装方式，这种方式适合东方超算系统。当前东方超算系统已提供NAMD的DTK版本，**无特殊需求用户可跳过安装，直接使用东方超算系统提供的版本**。

下载NAMD HIP版本源代码

```bash
wget https://www.ks.uiuc.edu/Research/namd/alpha/2.15_amdhip/download/NAMD_2.15a1_Source-AMDHIP.tgz
tar xzvf NAMD_2.15a1_Source-AMDHIP.tgz
cd NAMD_2.15a1_Source-AMDHIP
```

解压之后进入目录，查看到BuildInstructions.md，其中提供多节点版本安装参考方法。

下载tcl、fftw等依赖库

```bash
wget https://www.ks.uiuc.edu/Research/namd/libraries/fftw-linux-x86_64.tar.gz
wget https://www.ks.uiuc.edu/Research/namd/libraries/tcl8.5.9-linux-x86_64.tar.gz
```

在NAMD源码目录，执行以下命令（install.sh）安装charm++和NAMD软件

```bash
#!/bin/bash
module purge
module load compiler/devtoolset/7.3.1
module load mpi/hpcx/2.7.4/gcc-7.3.1
module load compiler/rocm/dtk/22.10.1
which hipcc
echo ${ROCM_PATH}

rm -rf Linux-x86_64-g++
rm -rf charm-v6.10.2
rm -rf charm
rm -rf fftw
rm -rf tcl
rm -rf tcl-threaded

tar -xvf charm-6.10.2.tar
tar -xvf fftw-linux-x86_64.tar.gz
tar -xvf tcl8.5.9-linux-x86_64.tar.gz
tar -xvf tcl8.5.9-linux-x86_64-threaded.tar.gz
ln -s charm-v6.10.2/ charm
mv linux-x86_64/ fftw
mv tcl8.5.9-linux-x86_64 tcl #ln -s
mv tcl8.5.9-linux-x86_64-threaded tcl-threaded

cd charm
#charm++ for multiple nodes. 
./build charm++ ucx-linux-x86_64 ompipmix smp --with-production --enable-error-checking  --basedir=/opt/hpc/software/mpi/pmix --basedir=/opt/hpc/software/mpi/hpcx/v2.4.1/ucx_without_rocm --basedir=/opt/hpc/software/mpi/hpcx/v2.4.1/gcc-7.3.1/ -j4

cd ..
#build NAMD
./config  Linux-x86_64-g++ --charm-arch ucx-linux-x86_64-ompipmix-smp --with-hip --rocm-prefix ${ROCM_PATH} --hipcub-prefix ${ROCM_PATH}/hipcub --rocprim-prefix ${ROCM_PATH}/rocprim
cd Linux-x86_64-g++/
make -j4
make release #move target files into release DIR
```

以上配置选择了DTK 22.10.1版本，可以根据需要改为其他DTK或ROCm版本。

在`make release`之后会在`Linux-x86_64-g++`目录产生`NAMD_2.15alpha1_Linux-x86_64-ucx-smp-HIP`目录，其中包括NAMD运行的bin和lib等文件，可以单独拷贝出来使用。这里将其拷贝（安装）到~/local目录，并新建modulefile文件（可选）：

```bash
mv NAMD_2.15alpha1_Linux-x86_64-ucx-smp-HIP ~/local/ #move to install
mkdir -p ~/local/modulefiles/apps/NAMD/2.15
vi ~/local/modulefiles/apps/NAMD/2.15/ucx-smp-HIP-gcc-7.3.1-dtk-22.10.1
```

输入下面的modulefile文件内容

```tcl
#%Module1.0

set name "NAMD"
set ver "2.15alpha1"
set kern [ exec uname -s ]
module-whatis "Name        : $name"
module-whatis "Version     : $ver"
module-whatis "Description : $name $ver for $kern HIP"

#prereq
module load compiler/devtoolset/7.3.1
module load compiler/rocm/dtk/22.10.1
module load mpi/hpcx/2.7.4/gcc-7.3.1

conflict apps/NAMD
set base_dir  $::env(HOME)/local/NAMD_2.15alpha1_Linux-x86_64-ucx-smp-HIP
set pmix_home /opt/hpc/software/mpi/pmix
setenv NAMD_ROOT  $base_dir
prepend-path  LD_LIBRARY_PATH ${pmix_home}/lib
prepend-path  PATH $base_dir
```

以上DTK、GCC等软件版本对应NAMD安装时的版本。

用module加载环境

```
module purge
module use $HOME/local/modulefiles
module load apps/NAMD/2.15/ucx-smp-HIP-gcc-7.3.1-dtk-22.10.1
```

编译后的`NAMD_2.15alpha1_Linux-x86_64-ucx-smp-HIP`软件包和modulefile文件`ucx-smp-HIP-gcc-7.3.1-dtk-22.10.1`可用推荐给root安装在系统路径以便与其他用户共享。

## 3. 测试算例

提供了3个测试算例在tests目录，下载路径https://gitlab.com/orise/namd-hip/-/tree/master/tests 。

- 载脂蛋白apoa1，92,224个原子
- 三磷酸腺苷合酶f1atpase，237,506个原子
- 卫星烟草花叶病毒stmv，1,066,628个原子

其中包含作业提交脚本run.sh和.log日志输出文件样例。

修改每个算例脚本run.sh中的PATH路径（module load NAMD）指向NAMD安装路径。

以**apoa1**为例，其run.sh内容如下，根据需要可以修改适配。

```bash
#!/bin/bash
#SBATCH -J apoa1
#SBATCH -p normal
#SBATCH -N 1
#SBATCH -n 32
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log
date 
	
module purge
module load compiler/devtoolset/7.3.1
module load mpi/hpcx/2.7.4/gcc-7.3.1
module load compiler/rocm/dtk/22.10.1

export LD_LIBRARY_PATH=/opt/hpc/software/mpi/pmix/lib:$LD_LIBRARY_PATH
export PATH=$HOME/local/NAMD_2.15alpha1_Linux-x86_64-ucx-smp-HIP:$PATH
#or
#module use $HOME/local/modulefiles
#module load apps/NAMD/2.15/ucx-smp-HIP-gcc-7.3.1-dtk-22.10.1

export HIP_VISIBLE_DEVICES=1
mpirun  -np 1  namd2 apoa1.namd ++ppn 1 +setcpuaffinity +devices 0
```

使用sbatch提交作业

```
cd test/apoa1
sbatch run.sh
```

**注意**以上NAMD_2.15alpha1_Linux-x86_64-ucx-smp-HIP是个人在HOME本地编译的版本，可用通过export PATH方式调用，也可module load自定义moduefile 。

**用户也可直接用东方平台提供的已编译的NAMD版本**，使用module 加载，相应的提交脚本run.sh如

```bash
#!/bin/bash
#SBATCH -J apoa1
#SBATCH -p normal
#SBATCH -N 1
#SBATCH -n 32
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log
date 
	
module purge
module load apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1

export HIP_VISIBLE_DEVICES=1
mpirun  -np 1  namd2 apoa1.namd ++ppn 1 +setcpuaffinity +devices 0
```

注意`apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1` modulefile中已经添加了GCC、DTK等环境，通过以下命令确认

```
module show apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1
```

故不需重复添加，只需要`module load apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1`。

多进程或跨节点计算的配置示例如下

- 使用1个计算节点，该节点运行4个MPI rank，在每个MPI rank调用1块DCU卡：
```
#!/bin/bash
#SBATCH -J ps
#SBATCH -p normal
#SBATCH -N 1
#SBATCH --ntasks-per-node=4
#SBATCH -B 4:8
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log
date 
	
module purge
module load apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1

export HIP_VISIBLE_DEVICES=0,1,2,3
mpirun -np 4 namd2 apoa1.namd ++ppn 7 +setcpuaffinity +devices 0,1,2,3 +ignoresharing
```

- 使用4个计算节点，每个节点运行一个MPI rank，在每个节点中只调用1块DCU卡：
```
#!/bin/bash
#SBATCH -J ps
#SBATCH -p normal
#SBATCH -N 4
#SBATCH --ntasks-per-node=1
#SBATCH -B 4:8
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log
date 
	
module purge
module load apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1

export HIP_VISIBLE_DEVICES=0,1,2,3
mpirun  -np 4 namd2 apoa1.namd ++ppn 31 +setcpuaffinity +devices 0 +ignoresharing
```



## 4. 性能优化配置

东方超算系统每个CPU Socket由4个CPU Die组成，每个CPU Die中包含有8个CPU核，故每个计算节点一共32个CPU核；同时每个节点配有4块DCU（类GPU）加速卡，也即每个CPU DIE搭配一块DCU（使用rocm-smi查看）；而且每个节点有4个内存控制块（NUMA node，使用lscpu查看）。为了实现计算性能的提升，推荐应用程序运行测试使用进程数、线程数和DCU卡数的最优配置，测试NUMA绑定的性能提升，NAMD官方也告知了[性能提升的一般过程](https://www.ks.uiuc.edu/Research/namd/wiki/?NamdPerformanceTuning)。应用程序NUM绑定可以通过脚本 bind.sh实现。

```bash
#!/bin/bash
cmd=$*
#map local-rank to numa-node
numa_node=$(($OMPI_COMM_WORLD_LOCAL_RANK%4)) 
#export UCX_NET_DEVICES=mlx5_${numa_node}:1
#export UCX_IB_PCI_BW=mlx5_${numa_node}:50Gbs
export HIP_VISIBLE_DEVICES=${numa_node}
numactl -N ${numa_node} -m ${numa_node} ${cmd}
```

将这个脚本改为可执行属性`chmod +x bind.sh`。

相应的NAMD作业提交脚本run.sh内容改为

```bash
#!/bin/bash
#SBATCH -J apoa1
#SBATCH -p normal   
#SBATCH -N 1         
#SBATCH -B 4:8      
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log

hostfile=hostfile.${SLURM_JOB_ID}
for i in $(scontrol show hostnames $SLURM_NODELIST)
do
    echo "$i slots=4" >> $hostfile  
done

module purge
module load apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1

### 启用4进程，每进程1卡6线程，绑定NUMA的执行语句
mpirun  -np 4 ./bind.sh namd2 stmv.namd ++ppn 6 +setcpuaffinity +devices 0 +ignoresharing
### 启用4进程，每进程1卡6线程，不绑定NUMA的执行语句（去除下列注释符#开启）
#mpirun  -np 4  namd2  stmv.namd ++ppn 6 +setcpuaffinity +devices 0,1,2,3
```
绑定NUMA时调用了bind.sh脚本。以上参数相关说明为

- -J  apoa1表示作业名为apoa1
- -P normal 表示作业提交到normal分区
- -N 1 表示节点数，这里为申请1个计算节点
- -w h03r1n05 表示每次运行都在h03r1n05空闲节点上，多个节点用逗号隔开。在性能测试时，指定每次运行都固定节点上，便于性能结果对比，通过sinfo查看可用的空闲节点。
- -B 4:8  表示每个节点配置4个DIE，每个DIE有8个core，这些配置参数只能小于物理装备数量
- --exclusive 表示本作业在每个计算节点上独占运行，不与其他用户作业共享计算节点
- --gres=dcu:4  每个节点配置4 个DCU卡（注意：-B 4:8与dcu:4相对应）
- export HIP_VISIBLE_DEVICES=2 表示当前进程可见（可用）的DCU编号（不是数量），每个节点4块DCU，编号从0到3。当前进程多个可见编号用逗号隔开，如HIP_VISIBLE_DEVICES=1,2,3

- +devices 0 +ignoresharing：在bind.sh(NUMA绑定脚本)中，使用HIP_VISIBLE_DEIVCES指定每个进程对应一个设备id，相当于每个进程只能看到1块卡，对于每个进程来说，当前的DCU卡都是编号为0，所以指定成+devices 0；
- 执行命令中，申请了4块DCU卡，启动4个进程，而此时使用了NUMA绑定，每个进程只能看到device 0，为了防止每个进程都重复执行在device 0上，加上+ignoresharing参数。
- ++ppn 6 ：++ppn代表每个进程执行的6个线程数，而NAMD内部隐含用另一个进程用于通信(comm)，即每个节点用了6 + 1=7个线程，因为每个卡申请了8个线程核，故还留有一个线程是为了其它的线程使用，防止抢占资源导致等待。
- +setcpuaffinity 选项是为了CPU核绑定，这样进程调度不会在CPU核上切换，即进程固定在某个CPU核执行，节省CPU调度切换的开销。
- 不绑定NUMA时使用+devices 0,1,2,3，是因为在执行时，未使用NUMA绑定（NUMA绑定中设置了HIP_VISIBLE_DEVICES），所以在使用4个进程时，需要+devices 0,1,2,3

针对具体的应用实例，用户通过选择不同的节点数和进程数配置，测试具体应用的计算性能（NAMD log会输出性能指标），选择一个性价比最优的参数配置，下表列出了一些可选的运行测试参数。

| 节点数 | 进程数 | NUMA bind                         | NUMA free                | 说明               |
| ------ | ------ | --------------------------------- | ------------------------ | ------------------ |
| -N 1   | -np 1  | ++ppn 7 +devices 0 +ignoresharing | ++ppn 7 +devices 0       | 单节点1进程1 DCU卡 |
| -N 1   | -np 2  | ++ppn 6 +devices 0 +ignoresharing | ++ppn 6 +devices 0,1     | 单节点2进程2 DCU卡 |
| -N 1   | -np 3  | ++ppn 6 +devices 0 +ignoresharing | ++ppn 6 +devices 0,1,2   | 单节点3进程3 DCU卡 |
| -N 1   | -np 4  | ++ppn 6 +devices 0 +ignoresharing | ++ppn 6 +devices 0,1,2,3 | 单节点4进程4 DCU卡 |
| -N 2   | -np 2  | ++ppn 7 +devices 0 +ignoresharing | ++ppn 7 +devices 0       | 每节点1进程1 DCU卡 |
| -N 2   | -np 4  | ++ppn 6 +devices 0 +ignoresharing | ++ppn 6 +devices 0,1     | 每节点2进程2 DCU卡 |
| -N 2   | -np 8  | ++ppn 6 +devices 0 +ignoresharing | ++ppn 6 +devices 0,1,2,3 | 每节点4进程4 DCU卡 |

## 5. 实例性能测试

我们针对四个实例（见附件`tests`目录）在单节点上测试不同线程和DCU卡组合的性能数据，以stmv实例为例，提交脚本为

```
#!/bin/bash
#SBATCH -J stmv
#SBATCH -p normal   
#SBATCH -N 1         
#SBATCH --ntasks-per-node=1
#SBATCH -B 4:8      
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log

env|grep SLURM
srun hostname |sort|awk 'NF==1{arr[$1]++} END{for (i in arr){printf "%s:%d\n",i,arr[i]}}'
export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export HIP_VISIBLE_DEVICES=0,1,2,3
module purge
#module load apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1
module load apps/NAMD-DCU/3.0/dtk-22.10

mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 4 +setcpuaffinity +devices 0  stmv.namd
mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 16 +setcpuaffinity +devices 0  stmv.namd
mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 31 +setcpuaffinity +devices 0  stmv.namd

mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 31 +setcpuaffinity +devices 0  stmv.namd 
mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 31 +setcpuaffinity +devices 0,1  stmv.namd 
mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 31 +setcpuaffinity +devices 0,1,2,3  stmv.namd 
```

这里使用NAMD 3.0其命令行namd2是namd3的软链接文件。这里通过`++ppn 31` 指定31 各worker threads (PEs) 为31，加上NAMD隐含的 1个comm thread一共32个线程。

在单计算节点上，使用多线程并行同时搭配相应的DCU加速计算，性能结果如下各表。对于步长（TIMESTEP）为1 fs时，单位变换关系为 `1 s/step=10^6/86400 days/ns `。

**APOA1**测试体系参数包含原子数92224，步长1fs，运行步数5000。

| 线程数 | DCU卡数 | s/step   | days/ns |
| ------ | ------- | -------- | ------- |
| 1      | 1       | 0.025144 | 0.29101 |
| 4      | 1       | 0.009305 | 0.10769 |
| 16     | 1       | 0.005486 | 0.06349 |
| 31     | 1       | 0.004952 | 0.05731 |
| 31     | 2       | 0.008225 | 0.09519 |
| 31     | 4       | 0.017029 | 0.19709 |

**F1ATPASE**测试体系参数包含原子数327506，步长1fs，运行步数5000。

| 线程数 | DCU卡数 | s/step   | days/ns |
| ------ | ------- | -------- | ------- |
| 1      | 1       | 0.087785 | 1.01603 |
| 4      | 1       | 0.033009 | 0.38204 |
| 16     | 1       | 0.018963 | 0.21947 |
| 31     | 1       | 0.018622 | 0.21553 |
| 31     | 2       | 0.017481 | 0.20232 |
| 31     | 4       | 0.020446 | 0.23664 |

**STMV**测试体系参数包含原子数1066628，步长1fs，运行步数5000。

| 线程数 | DCU卡数 | s/step   | days/ns |
| ------ | ------- | -------- | ------- |
| 1      | 1       | 0.357573 | 4.13857 |
| 4      | 1       | 0.131661 | 1.52385 |
| 16     | 1       | 0.073035 | 0.84531 |
| 31     | 1       | 0.064622 | 0.74793 |
| 31     | 2       | 0.050272 | 0.58185 |
| 31     | 4       | 0.046793 | 0.54158 |

**PS16**测试体系参数包含原子数1641138，步长1fs，运行步数10000。

| 线程数 | DCU卡数 | s/step   | days/ns |
| ------ | ------- | -------- | ------- |
| 1      | 1       | 0.737622 | 8.53729 |
| 4      | 1       | 0.253114 | 2.92956 |
| 16     | 1       | 0.13209  | 1.52881 |
| 31     | 1       | 0.117003 | 1.35420 |
| 31     | 2       | 0.092464 | 1.07019 |
| 31     | 4       | 0.078175 | 0.90480 |

所有任务运行完毕后，通过log日志提取性能数据，以列表方式给出各个实例的最佳性能结果如：

| Case     | atoms   | DCUs | s/step   | days/ns |
| -------- | ------- | ---- | -------- | ------- |
| APOA1    | 92224   | 1    | 0.004952 | 0.05731 |
| F1ATPASE | 327506  | 2    | 0.017481 | 0.20232 |
| STMV     | 1066628 | 4    | 0.046793 | 0.54158 |
| PS16     | 1641138 | 4    | 0.078175 | 0.90480 |

以上四个体系最大包含164万原子数，使用单DCU卡加速计算强度与显存（16GB）大小能够满足一般MD计算需求，测试显示单节点上使用单个DCU卡时，并开启该节点所有线程性能一般达到最佳；使用单节点多DCU卡计算时，增加了跨DCU的并行任务划分，相应的通讯开销也增加了，导致多卡计算性能并不一定高于单卡加速情况，对于大体系多卡加速可能有绝对性能的提升。

另外，东方超算上所编译的NAMD支持跨节点，每个节点都会调用对应节点上的多个DCU卡。但对于百万原子数以下的（中小）体系，一般单节点就可以达到最佳性能，跨节点总体性能会较低。
