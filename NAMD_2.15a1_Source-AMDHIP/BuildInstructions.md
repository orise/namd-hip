### Setup
Download tarballs and prerequisites, like the tcl/fftw libraries that make everyone's life easier.
```bash
cd NAMD
wget https://www.ks.uiuc.edu/Research/namd/libraries/fftw-linux-x86_64.tar.gz
wget https://www.ks.uiuc.edu/Research/namd/libraries/tcl8.5.9-linux-x86_64.tar.gz
tar -zxf fftw-linux-x86_64.tar.gz
tar -zxf tcl8.5.9-linux-x86_64.tar.gz
tar -xf charm-6.10.2.tar
mv linux-x86_64 fftw
ln -s tcl8.5.9-linux-x86_64 tcl
ln -s charm-v6.10.2 charm
cd charm
#charm++ for single node
./build charm++ multicore-linux-x86_64   -j16  --with-production
#charm++ for multinode. This is basically a safe fallback that depends on nothing to function.
#You can change these options to get MPI+SMP or UCX or whatever else strikes your fancy.
#Just running ./build on its own will get you an interactive option selector.
./build charm++ netlrts-linux-x86_64   smp  -j16  --with-production
cd ..
```

### Single Node Build
Single node builds use the multicore charm++ version. `--with-hip` signals to use HIP for GPU accleration, and this works with a number of different compilers. Here is a demonstration with gcc:
```bash
./config Linux-x86_64-g++.hip --with-hip --charm-arch multicore-linux-x86_64
cd Linux-x86_64-g++.hip
make -j8
#This directory should now contain a namd2 binary.
```
If you prefer clang, that works too.
```bash
./config Linux-x86_64-clang++.hip --with-hip --charm-arch multicore-linux-x86_64
cd Linux-x86_64-clang++.hip
make -j8
#This directory should now contain a namd2 binary.
```
If you'd like to use nvcc to compile following the HIP codepaths, add both `--with-hip` and `--with-cuda` to the config line.
```bash
./config Linux-x86_64-g++.hip+cuda --with-hip --with-cuda --charm-arch multicore-linux-x86_64
cd Linux-x86_64-g++.hip+cuda
make -j8
#This directory should now contain a namd2 binary.
```

The single node builds are run like any other binary.
```bash
/path/to/namd/namd2 +p8 configfile.namd > logfile.log
```

### Multiple Node Build
The big difference is that you need to specify a charm++ architecture that can communicate across nodes. I'll demonstrate with netlrts. Note that NAMD recommends/mandates SMP mode when using GPUs.
```bash
./config Linux-x86_64-g++.netlrts+hip --with-hip --charm-arch netlrts-linux-x86_64-smp
cd Linux-x86_64-g++.netlrts+hip
make -j8
#This directory should now contain a namd2 and charmrun binary.
```
Clang versions would be similarly built.
```bash
./config Linux-x86_64-clang++.netlrts+hip --with-hip --charm-arch netlrts-linux-x86_64-smp
cd Linux-x86_64-clang++.netlrts+hip
make -j8
#This directory should now contain a namd2 and charmrun binary.
```
These multinode builds are usually run with charmrun. An example where you pretend a single node is really multiple nodes would be:
```bash
DIRPATH=/path/to/namd/and/charmrun
$DIRPATH/charmrun ++local ++verbose +p24 $DIRPATH/namd2 ++ppn 12 +ignoresharing configfile.namd > logfile.log
```

Doing proper multinode runs (without `++local`), the runs would be something like this within an interactive allocation:
```bash
scontrol show hostname > hostfile
sed -i -e 's/^/host /' hostfile
#Add the setup so that each host has the modules
sed -i -e 's/$/ setup module load rocm\/3.7.0/' hostfile
sed -i '1s/^/group main \n/' hostfile
DIRPATH=/path/to/namd/and/charmrun
$DIRPATH/charmrun ++verbose +p24 ++ppn 12 ++nodelist hostfile $DIRPATH/namd2 +ignoresharing configfile.namd > logfile.log
```

### Multiple ROCm version demonstration
On some machines, multiple ROCm versions are installed, and something like this can be used to quickly build multiple NAMD variants with different ROCm versions by exploiting the `--rocm-prefix` option.
```bash
for rocmv in 3.3.0 3.5.0 3.6.0 3.7.0
do
	nodots="${rocmv//./}"
	module load rocm/$rocmv
	./config Linux-x86_64-g++.hip$nodots --with-hip --charm-arch multicore-linux-x86_64 --rocm-prefix /opt/rocm-$rocmv
	cd Linux-x86_64-g++.hip$nodots
	make -j8
	cd ..
	./config Linux-x86_64-clang++.hip$nodots --with-hip --charm-arch multicore-linux-x86_64 --rocm-prefix /opt/rocm-$rocmv
	cd Linux-x86_64-clang++.hip$nodots
	make -j8
	cd ..
	module unload rocm/$rocmv
done
```

### Adding in profiling information

NAMD has some profiling already built into it, main around the primary force integration that happens on the CPU. This can be activated by adding the `--with-cuda-profiling` or `--with-rocm-profiling` to the NAMD config line.
The upshot of this extra work is that you can see patterns in the execution flow with `rocprof`. My favorite line for this is `rocprof --hip-trace --roctx-trace --stats ../NAMD/Linux-x86_64-g++.hipprof/namd2 +p1 apoa1.namd`
