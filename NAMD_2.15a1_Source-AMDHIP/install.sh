#!/bin/bash
module purge
module load compiler/devtoolset/7.3.1
module load mpi/hpcx/2.7.4/gcc-7.3.1
module load compiler/rocm/dtk/22.10.1
which hipcc
echo ${ROCM_PATH}

rm -rf Linux-x86_64-g++
rm -rf charm-v6.10.2
rm -rf charm
rm -rf fftw
rm -rf tcl
rm -rf tcl-threaded

tar -xvf charm-6.10.2.tar
tar -xvf fftw-linux-x86_64.tar.gz
tar -xvf tcl8.5.9-linux-x86_64.tar.gz
tar -xvf tcl8.5.9-linux-x86_64-threaded.tar.gz
ln -s charm-v6.10.2/ charm
mv linux-x86_64/ fftw
mv tcl8.5.9-linux-x86_64 tcl #ln -s
mv tcl8.5.9-linux-x86_64-threaded tcl-threaded

cd charm
#charm++ for multiple nodes. 
./build charm++ ucx-linux-x86_64 ompipmix smp --with-production --enable-error-checking  --basedir=/opt/hpc/software/mpi/pmix --basedir=/opt/hpc/software/mpi/hpcx/v2.4.1/ucx_without_rocm --basedir=/opt/hpc/software/mpi/hpcx/v2.4.1/gcc-7.3.1/ -j4

cd ..
#build NAMD
./config  Linux-x86_64-g++ --charm-arch ucx-linux-x86_64-ompipmix-smp --with-hip --rocm-prefix ${ROCM_PATH} --hipcub-prefix ${ROCM_PATH}/hipcub --rocprim-prefix ${ROCM_PATH}/rocprim
cd Linux-x86_64-g++/
make -j4
make release #move target files into release DIR