#!/bin/bash
#SBATCH -J ps
#SBATCH -p normal   
#SBATCH -N 1         
#SBATCH --ntasks-per-node=1
#SBATCH -B 4:8      
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log


env|grep SLURM
srun hostname |sort|awk 'NF==1{arr[$1]++} END{for (i in arr){printf "%s:%d\n",i,arr[i]}}'
export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export HIP_VISIBLE_DEVICES=0,1,2,3
module purge
#module load apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1
module load apps/NAMD-DCU/3.0/dtk-22.10

#module use $HOME/local/modulefiles
#module load apps/NAMD/2.15/ucx-smp-HIP-gcc-7.3.1-dtk-22.10.1

#mpirun  -np 4 ./bind.sh namd2 stmv.namd ++ppn 6 +setcpuaffinity +devices 0 +ignoresharing
mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 1 +setcpuaffinity +devices 0  ps.namd 
mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 4 +setcpuaffinity +devices 0  ps.namd
mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 16 +setcpuaffinity +devices 0  ps.namd
mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 31 +setcpuaffinity +devices 0  ps.namd

mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 31 +setcpuaffinity +devices 0,1  ps.namd 
mpirun  -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 31 +setcpuaffinity +devices 0,1,2,3  ps.namd 

rm -rf FFTW_NAMD_*.txt *.BAK
#grep -E "Running|TIMING|binding"  ps-*.log
