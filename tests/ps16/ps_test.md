# 东方超算NAMD实例测试性能分析

2023年8月28日，徐顺， xushun@sccas.cn

**实例配置参数：**

```tcl
[xushun@login10 c36_ps]$ cat equ.conf
#############################################################
## JOB DESCRIPTION                                         ##
#############################################################

# Minimization and Heating of Protein in a Water Box

#############################################################
## ADJUSTABLE PARAMETERS                                   ##
#############################################################

firsttimestep      10000     # number of the first timestep, not 0 if a continuation simulation

#############################################################
## Input and Output Parameter                              ##
#############################################################

#input
structure           model/ps2.psf     # Protein Structure File
coordinates         model/ps2.pdb     # Protein Coordinates File
binvelocities       equ3/ps2.equ3-restart.vel     # restart velocity file
bincoordinates      equ3/ps2.equ3-restart.coor    # restart Coordinates File

#output
#outputName         ps2.equ4  # base name for output, input from cmd line
binaryoutput       off       # close the binary output files
#restartname        outputname.restart   # base name for restart files
restartfreq        10000       # restart files freq
binaryrestart      yes       # save binary restart file
#dcdfile            outputname.dcd  # coordinate trajectory output file name
#veldcdfile         outputname.veldcd   # velocity trajectory output file name
veldcdfreq         50000       # frequency to write velocity to the trajectory
dcdfreq            10000       # frequency to write coordinate to the trajectory
outputEnergies     10000       # frequency to write energy to stdout
outputMomenta      10000       # frequency to write momentum to stdout
outputPressure     10000       # frequency to write Pressure to stdout
outputTiming       10000       # frequency to write performace information to stdout

#############################################################
## SIMULATION PARAMETERS                                   ##
#############################################################
#Force field
paraTypeCharmm     on        # which FF is used, X-plor, Charmm or something else
parameters        toppar_c36_jul21/par_all36m_prot.prm
parameters        toppar_c36_jul21/par_all36_carb.prm
parameters        toppar_c36_jul21/par_all36_lipid.prm
parameters        toppar_c36_jul21/par_all36_na.prm
parameters        toppar_c36_jul21/par_all36_cgenff.prm
parameters        toppar_c36_jul21/par_cofactor.prm
parameters        toppar_c36_jul21/toppar_water_ions.str
parameters        toppar_c36_jul21/toppar_all36_prot_na_combined.str
parameters        toppar_c36_jul21/toppar_all36_prot_retinol.str
parameters        toppar_c36_jul21/toppar_all36_prot_heme.str
parameters        toppar_c36_jul21/toppar_all36_carb_glycopeptide.str
parameters        toppar_c36_jul21/toppar_all36_lipid_cholesterol.str
parameters        toppar_c36_jul21/toppar_all36_lipid_sphingo.str
parameters        toppar_c36_jul21/toppar_all36_lipid_inositol.str
parameters        toppar_c36_jul21/toppar_all36_carb_glycolipid.str

exclude            scaled1-4 # Force-Field Parameters
1-4scaling         1.0       # 1-4 scaleing when exclude is set to scaled1-4
cutoff             12.0      # cut off value of the non-bond interaction
switching          on        # open the switching method for the non-bond interaction
switchdist         10.0      # distance begin to switch
pairlistdist       14.0      # distance to make atom interaction pair list

# Basic dynamics
COMmotion          no        # remove the COM motion in the initial step
dielectric         1.0       # Dieletric constant for the system

# Integrator Parameters, timestep parameters
timestep           1.0       # 2fs timestep
rigidBonds         none      # bond including H is constrained
nonbondedFreq      1         # number of timesteps between nonbonded evaluation
fullElectFrequency 2         # number of timesteps between full electrostatic evaluations
stepspercycle      20        # number of timesteps in each atom reassignments cycle
molly              off       # close the molly method, not compatible with Shake.
#CUDASOAintegrate   on

#fix atoms
#fixedAtoms         on        # open to fix atoms
#fixedAtomsForces   on        # needed by turn fixed atoms off among simulation
#fixedAtomsFile     em-5th-fix.pdb   # reference structure
#fixedAtomsCol      B         # set beta non-zero fo fix the atoms

# Harmonic constraint parameters^M
constraints        on        # make constraints active
consRef            model/constrains.pdb     # PDB constraint reference positions
consKFile          model/constrains.pdb     # PDB file containing force constant values
consKCol           B         # PDB column including force constant

# Periodic Boundary Conditions
#cellBasisVector1  113.217    0.       0.    # basis vector (A)
#cellBasisVector2    0.     113.217    0.
#cellBasisVector3    0.       0.     173.048
#cellOrigin          0.       0.      40.  # center for periodic cell
extendedSystem     equ3/ps2.equ3-restart.xsc             # Periodic cell parameter is read from this file
#XSTfile           outputname.xst             # a file include the a record of the periodic cell parameters
XSTfreq            10000               # frequency to write to the *.xst file
#wrapWater          on                # wrap all water coordinates around periodic boundaries
wrapAll             on               # wrap all coordinates around periodic boundaries
#wrapNearest        off               # wrapped to nearest image to the center

# PME (for full-system periodic electrostatics)
PME                yes       # open the PME to get eletrostatics interaction
PMEInterpOrder      4        # PME interpolation order
#PMEGridSizeX       64        # number of grid points in 3 dimensions
#PMEGridSizeY       64
#PMEGridSizeZ       64
PMEGridSpacing      1.0

# Temperature Control and equilibration
#temperature        0     # variable temperature used below
langevin           on     # do langevin dynamics
langevinDamping    0.1    # damping coefficient (gamma) of 5/ps
langevinTemp       310
langevinHydrogen   on     # don't couple langevin bath to hydrogens

# Constant Pressure Control (variable volume)
useGroupPressure      yes     # needed for rigidBonds
useFlexibleCell       yes     # use anisotropic cell fluctuation
useConstantRatio      yes     # constant shape in first two cell dimensions(X-Y)
#useConstantArea       no      # keep x-y plane constant while fluctuate s along Z
langevinPiston        on      # use Langevin piston  pressure control
langevinPistonTarget  1.01325 # Target pressure, in bar -> 1 atm
langevinPistonPeriod  200.    # Oscillation period (fs)
langevinPistonDecay   100.    # damping time scale (fs)
langevinPistonTemp    310     # noise temperature

#############################################################
## Special SIMULATION - SMD , IMD                          ##
#############################################################

#interactive molecular dynamics
IMDon             off        # open the IMD
#IMDprot          3000       # portNumber, used by VMD
#IMDFreq          1000       # send every 1000 setps
#IMDwait          no         # wait for VMD to connect before running
#IMDignore        no         # if yes, ignore any steering forces by VMD

#Steered molecular dynamics
SMD               off        # open to perform the SMD
#SMDfile                  smd.ref    # SMD constraint position, using nonzero occupancy column
#SMDK             100        # force constant(kcal/mol/A^2), 1kcal/mol/A^2=69.48pN/A
#SMDvel           0.001      # vellocity of SMD, A per timestep
#SMDdir           0.197 -0.949 -0.245   # direction of SMD center of mass movement
#SMDoutputfreq    500        # frequency of SMD output

#############################################################
## EXECUTION SCRIPT                                        ##
#############################################################

# run one step to get into scripting mode
#minimize 0

# turn off until later
#langevinPiston  off

# minimize nonbackbone atoms
#minimize 5000
#output min_fix

# min all atoms
#fixedAtoms     off
#minimize 5000
#output min_cons

# heat with CAs restrained
# langevin      on
#constraintScaling      0.5
#minimize 5000

# equilibrate volume with CAs restrained
#langevinPiston on
#run 1000000
#output equil_ca

# equilibrate volume without restraints
#constraintScaling      0.5
run 100000
```

**实例提交脚本：**

```bash
#!/bin/bash
#SBATCH -J ps
#SBATCH -p normal
#SBATCH -N 1
#SBATCH -B 4:8
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log

env|grep SLURM
srun hostname |sort|awk 'NF==1{arr[$1]++} END{for (i in arr){printf "%s:%d\n",i,arr[i]}}'
export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export HIP_VISIBLE_DEVICES=0,1,2,3
module purge
#module load apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1
module load apps/NAMD-DCU/3.0/dtk-22.10

### 启用1进程，每进程启动4线程，每线程绑定1个DCU卡，绑定NUMA的执行语句
#mpirun -np 1 namd2 --outputname ${NAME_PREFIX} ++ppn 4 +setcpuaffinity +devices 0,1,2,3 equ.conf
### 启用1进程，每进程启动4线程，其中1个线程绑定1个DCU卡，其他线程通过该线程访问DCU卡，绑定NUMA的执行语句
mpirun -np 1  namd2 --outputname ${NAME_PREFIX} ++ppn 4 +setcpuaffinity +devices 0 equ.conf

rm -rf FFTW_NAMD_*.txt *.BAK
```

命令行选项+devices不给出时，会提示

```
Did not find +devices i,j,k,... argument, using all
```

即默认使用当前节点所有DCU卡。

如果隐藏GPU设备，即`export HIP_VISIBLE_DEVICES=`，则也会报如下找不到设备的错误：

```
on Pe 21 (g11r3n10): no CUDA-capable device is detected
```

**测试结果及其分析**

测试体系参数包含原子数1641138，利用Charm力场， 步长1fs，运行步数100000。在单个计算节点上，使用多线程并行同时搭配相应的DCU加速计算，DCU卡是一种国产类GPU计算卡。实例性能结果如表：

| 线程数 | DCU卡数 | s/step    | days/ns |
| ------ | ------- | --------- | ------- |
| 1      | 1       | 0.730721  | 8.45741 |
| 4      | 1       | 0.252376  | 2.92101 |
| 16     | 1       | 0.131114  | 1.51752 |
| 31     | 1       | 0.119509  | 1.38320 |
| 2      | 2       | 0.425038  | 4.91942 |
| 4      | 2       | 0.236789  | 2.74061 |
| 16     | 2       | 0.0950335 | 1.09992 |
| 30     | 2       | 0.0793691 | 0.91862 |
| 4      | 4       | 0.210827  | 2.44012 |
| 31     | 4       | 0.080305  | 0.92945 |

由于体系包含164万原子数，使用单DCU卡加速计算强度与显存（16GB）大小能够满足一般MD计算需求，测试显示单节点上使用单个DCU卡时，并开启该节点所有线程性能最好达到 0.119509 s/step（即1.38320 days/ns)。由于这里步长为1fs，故 `1 s/step=10^6/86400 days/ns `。使用单节点多DCU卡计算时，增加了跨DCU的并行任务划分，相应的通讯开销也增加了，导致多卡计算性能并不一定高于单卡加速情况。<u>对于这个测试实例，单节点使用4块DCU卡并不一定会好于单卡，最优计算性能为30线程2块DCU卡加速的情况，计算性能为0.0793691 s/step，但从性价比上比较，使用单卡最好。</u>

也可以提交NAMD跨节点作业时，使得每个节点都调用DCU卡加速，每个节点可调用1块或多块DCU卡。

跨节点提交脚本如

```bash
#!/bin/bash
#SBATCH -J ps
#SBATCH -p normal
#SBATCH -N 4
#SBATCH --ntasks-per-node=1
#SBATCH -B 4:8
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log

env|grep SLURM
srun hostname |sort|awk 'NF==1{arr[$1]++} END{for (i in arr){printf "%s:%d\n",i,arr[i]}}'
export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export HIP_VISIBLE_DEVICES=0,1,2,3
module purge
#module load apps/NAMD-DCU/2.15/2.15-hpcx-gcc-7.3.1
module load apps/NAMD-DCU/3.0/dtk-22.10

mpirun -np 4  namd2 --outputname ${NAME_PREFIX} ++ppn 6 +setcpuaffinity +devices 0 equ.conf
```

下面是跨节点相对单节点的计算性能对比：

| 节点数 | 线程数/节点 | DCU卡/节点 | s/step    | days/ns |
| ------ | ----------- | ---------- | --------- | ------- |
| 1      | 31          | 1          | 0.119509  | 1.38320 |
| 4      | 6           | 1          | 0.0911148 | 1.05456 |

总来说NAMD跨节点并行效率很低，绝对速度有可能有一些提高，但所用计算资源很多，即性价比不高。故从性价比上考虑，通常在单节点上调用DCU加速更好。 

**测试实例日志性能结果提起**

```bash
[xushun@login10 c36_ps]$ grep -E "Running|TIMING|binding"  ps-*.log
ps-9386212.log:Charm++> Running in SMP mode: 1 processes, 31 worker threads (PEs) + 1 comm threads per process, 31 PEs total
ps-9386212.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9386212.log:Pe 16 physical rank 16 binding to CUDA device 0 on h04r2n06: 'Device 66a1'  Mem: 16368MB  Rev: 9.0  PCI: 0:26:0
ps-9386212.log:Info: Running on 31 processors, 1 nodes, 1 physical nodes.
ps-9386212.log:Info: TIMING OUTPUT STEPS    10000
ps-9386212.log:TCL: Running for 100000 steps
ps-9386212.log:TIMING: 20000  CPU: 1208.11, 0.120613/step  Wall: 1223.76, 0.122173/step, 3.05433 hours remaining, 4235.226562 MB of memory in use.
ps-9386212.log:TIMING: 30000  CPU: 2421.91, 0.121381/step  Wall: 2453.94, 0.123018/step, 2.73374 hours remaining, 4574.496094 MB of memory in use.
ps-9386212.log:TIMING: 40000  CPU: 3636.56, 0.121465/step  Wall: 3683.73, 0.122979/step, 2.39125 hours remaining, 4755.660156 MB of memory in use.
ps-9386212.log:TIMING: 50000  CPU: 4841.41, 0.120485/step  Wall: 4905.02, 0.122129/step, 2.03548 hours remaining, 4785.062500 MB of memory in use.
ps-9386212.log:TIMING: 60000  CPU: 6049.27, 0.120786/step  Wall: 6129.6, 0.122458/step, 1.7008 hours remaining, 4818.179688 MB of memory in use.
ps-9386212.log:TIMING: 70000  CPU: 7252.77, 0.12035/step  Wall: 7349.28, 0.121968/step, 1.3552 hours remaining, 4833.667969 MB of memory in use.
ps-9386212.log:TIMING: 80000  CPU: 8454.4, 0.120163/step  Wall: 8568.3, 0.121903/step, 1.01585 hours remaining, 4844.843750 MB of memory in use.
ps-9386212.log:TIMING: 90000  CPU: 9655.2, 0.12008/step  Wall: 9786.58, 0.121827/step, 0.676819 hours remaining, 4853.007812 MB of memory in use.
ps-9386212.log:TIMING: 100000  CPU: 10848.8, 0.119359/step  Wall: 10998, 0.12114/step, 0.336501 hours remaining, 4858.183594 MB of memory in use.
ps-9386212.log:TIMING: 110000  CPU: 12043.9, 0.119509/step  Wall: 12210.5, 0.121252/step, 0 hours remaining, 4861.824219 MB of memory in use.
ps-9387293.log:Charm++> Running in SMP mode: 1 processes, 4 worker threads (PEs) + 1 comm threads per process, 4 PEs total
ps-9387293.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9387293.log:Pe 1 physical rank 1 binding to CUDA device 1 on g11r2n04: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:26:0
ps-9387293.log:Pe 2 physical rank 2 binding to CUDA device 2 on g11r2n04: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:43:0
ps-9387293.log:Pe 0 physical rank 0 binding to CUDA device 0 on g11r2n04: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9387293.log:Pe 3 physical rank 3 binding to CUDA device 3 on g11r2n04: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:63:0
ps-9387293.log:Info: Running on 4 processors, 1 nodes, 1 physical nodes.
ps-9387293.log:Info: TIMING OUTPUT STEPS    10000
ps-9387293.log:TCL: Running for 100000 steps
ps-9387293.log:TIMING: 20000  CPU: 2166.94, 0.216553/step  Wall: 2176.44, 0.217479/step, 5.43698 hours remaining, 109100048.000000 MB of memory in use.
ps-9387293.log:TIMING: 30000  CPU: 4274.15, 0.210721/step  Wall: 4292.38, 0.211594/step, 4.7021 hours remaining, 109176976.000000 MB of memory in use.
ps-9387293.log:TIMING: 40000  CPU: 6355.63, 0.208149/step  Wall: 6382.43, 0.209005/step, 4.06399 hours remaining, 109087952.000000 MB of memory in use.
ps-9387293.log:TIMING: 50000  CPU: 8430.84, 0.20752/step  Wall: 8466.27, 0.208384/step, 3.47306 hours remaining, 109087952.000000 MB of memory in use.
ps-9387293.log:TIMING: 60000  CPU: 10543.4, 0.211251/step  Wall: 10587.5, 0.212126/step, 2.9462 hours remaining, 109350096.000000 MB of memory in use.
ps-9387293.log:TIMING: 70000  CPU: 12624.7, 0.20813/step  Wall: 12677.4, 0.208991/step, 2.32212 hours remaining, 109350096.000000 MB of memory in use.
ps-9387293.log:TIMING: 80000  CPU: 14749.7, 0.212505/step  Wall: 14811.2, 0.213379/step, 1.77816 hours remaining, 109612240.000000 MB of memory in use.
ps-9387293.log:TIMING: 90000  CPU: 16876.7, 0.212704/step  Wall: 16947, 0.213578/step, 1.18654 hours remaining, 109612240.000000 MB of memory in use.
ps-9387293.log:TIMING: 100000  CPU: 18997.1, 0.212039/step  Wall: 19076.1, 0.212912/step, 0.591422 hours remaining, 109612240.000000 MB of memory in use.
ps-9387293.log:TIMING: 110000  CPU: 21105.4, 0.210827/step  Wall: 21193.1, 0.211701/step, 0 hours remaining, 109612240.000000 MB of memory in use.
ps-9387295.log:Charm++> Running in SMP mode: 1 processes, 4 worker threads (PEs) + 1 comm threads per process, 4 PEs total
ps-9387295.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9387295.log:Pe 0 physical rank 0 binding to CUDA device 0 on g11r3n03: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9387295.log:Pe 1 physical rank 1 binding to CUDA device 1 on g11r3n03: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:26:0
ps-9387295.log:Pe 2 physical rank 2 binding to CUDA device 2 on g11r3n03: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:43:0
ps-9387295.log:Pe 3 physical rank 3 binding to CUDA device 3 on g11r3n03: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:63:0
ps-9387295.log:Info: Running on 4 processors, 1 nodes, 1 physical nodes.
ps-9387295.log:Info: TIMING OUTPUT STEPS    10000
ps-9387295.log:TCL: Running for 100000 steps
ps-9387295.log:TIMING: 20000  CPU: 2101.39, 0.209998/step  Wall: 2111.33, 0.210978/step, 5.27445 hours remaining, 108723920.000000 MB of memory in use.
ps-9387295.log:TIMING: 30000  CPU: 4172.76, 0.207137/step  Wall: 4191.97, 0.208065/step, 4.62366 hours remaining, 108845744.000000 MB of memory in use.
ps-9387295.log:TIMING: 40000  CPU: 6283.34, 0.211058/step  Wall: 6312.07, 0.21201/step, 4.12241 hours remaining, 109107888.000000 MB of memory in use.
ps-9387295.log:TIMING: 50000  CPU: 8393.85, 0.211051/step  Wall: 8432.09, 0.212002/step, 3.53337 hours remaining, 108909824.000000 MB of memory in use.
ps-9387295.log:TIMING: 60000  CPU: 10512.8, 0.211892/step  Wall: 10560.4, 0.212829/step, 2.95596 hours remaining, 108986752.000000 MB of memory in use.
ps-9387295.log:TIMING: 70000  CPU: 12628.9, 0.211609/step  Wall: 12685.8, 0.212544/step, 2.3616 hours remaining, 109773776.000000 MB of memory in use.
ps-9387295.log:TIMING: 80000  CPU: 14741.9, 0.211307/step  Wall: 14808.3, 0.21225/step, 1.76875 hours remaining, 109773776.000000 MB of memory in use.
ps-9387295.log:TIMING: 90000  CPU: 16856.1, 0.211419/step  Wall: 16931.9, 0.212354/step, 1.17975 hours remaining, 109773776.000000 MB of memory in use.
ps-9387295.log:TIMING: 100000  CPU: 18978.8, 0.212266/step  Wall: 19063.9, 0.213207/step, 0.592242 hours remaining, 109773776.000000 MB of memory in use.
ps-9387295.log:TIMING: 110000  CPU: 21094.3, 0.211552/step  Wall: 21188.7, 0.212481/step, 0 hours remaining, 109773776.000000 MB of memory in use.
ps-9387296.log:Charm++> Running in SMP mode: 1 processes, 1 worker threads (PEs) + 1 comm threads per process, 1 PEs total
ps-9387296.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9387296.log:Pe 0 physical rank 0 binding to CUDA device 0 on g11r3n06: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9387296.log:Info: Running on 1 processors, 1 nodes, 1 physical nodes.
ps-9387296.log:Info: TIMING OUTPUT STEPS    10000
ps-9387296.log:TCL: Running for 100000 steps
ps-9387296.log:TIMING: 20000  CPU: 7283.82, 0.728094/step  Wall: 7309.92, 0.7307/step, 18.2675 hours remaining, 105247424.000000 MB of memory in use.
ps-9387296.log:TIMING: 30000  CPU: 14565.1, 0.728127/step  Wall: 14617.1, 0.730721/step, 16.2382 hours remaining, 105884000.000000 MB of memory in use.
ps-9387296.log:TIMING: 40000  CPU: 21854.9, 0.728978/step  Wall: 21932.6, 0.731543/step, 14.2244 hours remaining, 105969904.000000 MB of memory in use.
ps-9387296.log:TIMING: 50000  CPU: 29150.3, 0.72954/step  Wall: 29253.6, 0.7321/step, 12.2017 hours remaining, 106023776.000000 MB of memory in use.
ps-9387296.log:TIMING: 60000  CPU: 36448.4, 0.729816/step  Wall: 36577.3, 0.732374/step, 10.1719 hours remaining, 106138192.000000 MB of memory in use.
ps-9387296.log:TIMING: 70000  CPU: 43750.7, 0.730227/step  Wall: 43905.1, 0.732777/step, 8.14196 hours remaining, 106159552.000000 MB of memory in use.
ps-9387296.log:TIMING: 80000  CPU: 51055.3, 0.730457/step  Wall: 51235.3, 0.733026/step, 6.10855 hours remaining, 106178720.000000 MB of memory in use.
ps-9387296.log:TIMING: 90000  CPU: 58358.3, 0.7303/step  Wall: 58564.1, 0.732875/step, 4.07153 hours remaining, 106194080.000000 MB of memory in use.
ps-9387296.log:TIMING: 100000  CPU: 65660.8, 0.730255/step  Wall: 65892.2, 0.732815/step, 2.0356 hours remaining, 106204256.000000 MB of memory in use.
ps-9387296.log:TIMING: 110000  CPU: 72968, 0.730721/step  Wall: 73225.2, 0.733299/step, 0 hours remaining, 106210256.000000 MB of memory in use.
ps-9387297.log:Charm++> Running in SMP mode: 1 processes, 4 worker threads (PEs) + 1 comm threads per process, 4 PEs total
ps-9387297.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9387297.log:Pe 2 physical rank 2 binding to CUDA device 0 on h01r1n15: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9387297.log:Info: Running on 4 processors, 1 nodes, 1 physical nodes.
ps-9387297.log:Info: TIMING OUTPUT STEPS    10000
ps-9387297.log:TCL: Running for 100000 steps
ps-9387297.log:TIMING: 20000  CPU: 2536.34, 0.253415/step  Wall: 2547.19, 0.254487/step, 6.36217 hours remaining, 106265024.000000 MB of memory in use.
ps-9387297.log:TIMING: 30000  CPU: 5056.66, 0.252032/step  Wall: 5077.98, 0.253079/step, 5.62397 hours remaining, 106386848.000000 MB of memory in use.
ps-9387297.log:TIMING: 40000  CPU: 7576.98, 0.252032/step  Wall: 7608.73, 0.253075/step, 4.9209 hours remaining, 106648992.000000 MB of memory in use.
ps-9387297.log:TIMING: 50000  CPU: 10096.8, 0.251985/step  Wall: 10139, 0.25303/step, 4.21716 hours remaining, 106648992.000000 MB of memory in use.
ps-9387297.log:TIMING: 60000  CPU: 12617.9, 0.252106/step  Wall: 12670.5, 0.253145/step, 3.5159 hours remaining, 106725904.000000 MB of memory in use.
ps-9387297.log:TIMING: 70000  CPU: 15143.7, 0.252579/step  Wall: 15206.7, 0.25362/step, 2.818 hours remaining, 106725904.000000 MB of memory in use.
ps-9387297.log:TIMING: 80000  CPU: 17666.8, 0.252308/step  Wall: 17740.1, 0.253343/step, 2.11119 hours remaining, 106725904.000000 MB of memory in use.
ps-9387297.log:TIMING: 90000  CPU: 20189.3, 0.252249/step  Wall: 20273, 0.253288/step, 1.40716 hours remaining, 106988048.000000 MB of memory in use.
ps-9387297.log:TIMING: 100000  CPU: 22711.7, 0.252243/step  Wall: 22805.8, 0.25328/step, 0.703555 hours remaining, 106988048.000000 MB of memory in use.
ps-9387297.log:TIMING: 110000  CPU: 25235.4, 0.252376/step  Wall: 25340, 0.253419/step, 0 hours remaining, 106988048.000000 MB of memory in use.
ps-9387298.log:Charm++> Running in SMP mode: 1 processes, 16 worker threads (PEs) + 1 comm threads per process, 16 PEs total
ps-9387298.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9387298.log:Pe 8 physical rank 8 binding to CUDA device 0 on h01r2n01: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9387298.log:Info: Running on 16 processors, 1 nodes, 1 physical nodes.
ps-9387298.log:Info: TIMING OUTPUT STEPS    10000
ps-9387298.log:TCL: Running for 100000 steps
ps-9387298.log:TIMING: 20000  CPU: 1314.15, 0.131233/step  Wall: 1321.81, 0.131983/step, 3.29958 hours remaining, 110341360.000000 MB of memory in use.
ps-9387298.log:TIMING: 30000  CPU: 2642.12, 0.132797/step  Wall: 2656.81, 0.1335/step, 2.96667 hours remaining, 110418144.000000 MB of memory in use.
ps-9387298.log:TIMING: 40000  CPU: 3963.09, 0.132096/step  Wall: 3984.63, 0.132782/step, 2.58186 hours remaining, 110680288.000000 MB of memory in use.
ps-9387298.log:TIMING: 50000  CPU: 5279.06, 0.131598/step  Wall: 5307.44, 0.132282/step, 2.20469 hours remaining, 110680288.000000 MB of memory in use.
ps-9387298.log:TIMING: 60000  CPU: 6596.62, 0.131756/step  Wall: 6631.82, 0.132437/step, 1.83941 hours remaining, 110757216.000000 MB of memory in use.
ps-9387298.log:TIMING: 70000  CPU: 7912.93, 0.131631/step  Wall: 7954.95, 0.132314/step, 1.47015 hours remaining, 110757216.000000 MB of memory in use.
ps-9387298.log:TIMING: 80000  CPU: 9228.01, 0.131509/step  Wall: 9276.81, 0.132185/step, 1.10154 hours remaining, 110757216.000000 MB of memory in use.
ps-9387298.log:TIMING: 90000  CPU: 10542.1, 0.13141/step  Wall: 10597.7, 0.132093/step, 0.733851 hours remaining, 110757216.000000 MB of memory in use.
ps-9387298.log:TIMING: 100000  CPU: 11857, 0.131487/step  Wall: 11919.5, 0.132177/step, 0.367159 hours remaining, 110757216.000000 MB of memory in use.
ps-9387298.log:TIMING: 110000  CPU: 13168.1, 0.131114/step  Wall: 13237.5, 0.131801/step, 0 hours remaining, 110757216.000000 MB of memory in use.
ps-9389759.log:Charm++> Running in SMP mode: 1 processes, 2 worker threads (PEs) + 1 comm threads per process, 2 PEs total
ps-9389759.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9389759.log:Pe 0 physical rank 0 binding to CUDA device 0 on g16r4n02: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9389759.log:Pe 1 physical rank 1 binding to CUDA device 1 on g16r4n02: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:26:0
ps-9389759.log:Info: Running on 2 processors, 1 nodes, 1 physical nodes.
ps-9389759.log:Info: TIMING OUTPUT STEPS    10000
ps-9389759.log:TCL: Running for 100000 steps
ps-9389759.log:TIMING: 20000  CPU: 4163.41, 0.416085/step  Wall: 4178.57, 0.417583/step, 10.4396 hours remaining, 106931696.000000 MB of memory in use.
ps-9389759.log:TIMING: 30000  CPU: 8396.57, 0.423316/step  Wall: 8426.42, 0.424785/step, 9.43968 hours remaining, 107008688.000000 MB of memory in use.
ps-9389759.log:TIMING: 40000  CPU: 12647, 0.425038/step  Wall: 12691.5, 0.426512/step, 8.29328 hours remaining, 107008688.000000 MB of memory in use.
ps-9389760.log:Charm++> Running in SMP mode: 1 processes, 4 worker threads (PEs) + 1 comm threads per process, 4 PEs total
ps-9389760.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9389760.log:Pe 2 physical rank 2 binding to CUDA device 1 on h01r1n02: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:26:0
ps-9389760.log:Pe 1 physical rank 1 binding to CUDA device 0 on h01r1n02: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9389760.log:Info: Running on 4 processors, 1 nodes, 1 physical nodes.
ps-9389760.log:Info: TIMING OUTPUT STEPS    10000
ps-9389760.log:TCL: Running for 100000 steps
ps-9389760.log:TIMING: 20000  CPU: 2293.57, 0.229118/step  Wall: 2303.22, 0.230065/step, 5.75162 hours remaining, 107564064.000000 MB of memory in use.
ps-9389760.log:TIMING: 30000  CPU: 4577.21, 0.228364/step  Wall: 4596.03, 0.229281/step, 5.09514 hours remaining, 107641024.000000 MB of memory in use.
ps-9389760.log:TIMING: 40000  CPU: 6915.38, 0.233816/step  Wall: 6943.46, 0.234743/step, 4.56444 hours remaining, 107641024.000000 MB of memory in use.
ps-9389760.log:TIMING: 50000  CPU: 9250.65, 0.233527/step  Wall: 9288.04, 0.234459/step, 3.90764 hours remaining, 107686032.000000 MB of memory in use.
ps-9389760.log:TIMING: 60000  CPU: 11618.5, 0.236789/step  Wall: 11665.3, 0.23773/step, 3.30181 hours remaining, 107717936.000000 MB of memory in use.
ps-9389760.log:TIMING: 70000  CPU: 13989, 0.237042/step  Wall: 14046.1, 0.238074/step, 2.64527 hours remaining, 107717936.000000 MB of memory in use.
ps-9389761.log:Charm++> Running in SMP mode: 1 processes, 16 worker threads (PEs) + 1 comm threads per process, 16 PEs total
ps-9389761.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9389761.log:Pe 4 physical rank 4 binding to CUDA device 0 on h03r2n05: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9389761.log:Pe 8 physical rank 8 binding to CUDA device 1 on h03r2n05: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:26:0
ps-9389761.log:Info: Running on 16 processors, 1 nodes, 1 physical nodes.
ps-9389761.log:Info: TIMING OUTPUT STEPS    10000
ps-9389761.log:TCL: Running for 100000 steps
ps-9389761.log:TIMING: 20000  CPU: 964.058, 0.096229/step  Wall: 970.049, 0.0968175/step, 2.42044 hours remaining, 110219536.000000 MB of memory in use.
ps-9389761.log:TIMING: 30000  CPU: 1938.87, 0.0974815/step  Wall: 1950.42, 0.0980368/step, 2.1786 hours remaining, 110832576.000000 MB of memory in use.
ps-9389761.log:TIMING: 40000  CPU: 2906.49, 0.0967617/step  Wall: 2923.39, 0.0972972/step, 1.89189 hours remaining, 111356864.000000 MB of memory in use.
ps-9389761.log:TIMING: 50000  CPU: 3876.72, 0.0970225/step  Wall: 3898.98, 0.0975594/step, 1.62599 hours remaining, 111356864.000000 MB of memory in use.
ps-9389761.log:TIMING: 60000  CPU: 4835.85, 0.0959132/step  Wall: 4863.42, 0.0964441/step, 1.3395 hours remaining, 111433840.000000 MB of memory in use.
ps-9389761.log:TIMING: 70000  CPU: 5792.58, 0.0956736/step  Wall: 5825.4, 0.0961975/step, 1.06886 hours remaining, 111433840.000000 MB of memory in use.
ps-9389761.log:TIMING: 80000  CPU: 6754.72, 0.0962141/step  Wall: 6792.79, 0.0967393/step, 0.806161 hours remaining, 111433840.000000 MB of memory in use.
ps-9389761.log:TIMING: 90000  CPU: 7713.06, 0.0958337/step  Wall: 7756.35, 0.0963557/step, 0.535309 hours remaining, 111433840.000000 MB of memory in use.
ps-9389761.log:TIMING: 100000  CPU: 8672.68, 0.0959615/step  Wall: 8721.22, 0.0964869/step, 0.268019 hours remaining, 111433840.000000 MB of memory in use.
ps-9389761.log:TIMING: 110000  CPU: 9623.01, 0.0950335/step  Wall: 9676.78, 0.0955561/step, 0 hours remaining, 111433840.000000 MB of memory in use.
ps-9389762.log:Charm++> Running in SMP mode: 1 processes, 30 worker threads (PEs) + 1 comm threads per process, 30 PEs total
ps-9389762.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9389762.log:Pe 8 physical rank 8 binding to CUDA device 0 on h04r2n06: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9389762.log:Pe 16 physical rank 16 binding to CUDA device 1 on h04r2n06: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:26:0
ps-9389762.log:Info: Running on 30 processors, 1 nodes, 1 physical nodes.
ps-9389762.log:Info: TIMING OUTPUT STEPS    10000
ps-9389762.log:TCL: Running for 100000 steps
ps-9389762.log:TIMING: 20000  CPU: 793.467, 0.07918/step  Wall: 808.935, 0.0807117/step, 2.01779 hours remaining, 114588560.000000 MB of memory in use.
ps-9389762.log:TIMING: 30000  CPU: 1593.31, 0.0799846/step  Wall: 1624.11, 0.0815171/step, 1.81149 hours remaining, 114672960.000000 MB of memory in use.
ps-9389762.log:TIMING: 40000  CPU: 2408.81, 0.0815499/step  Wall: 2453.64, 0.0829531/step, 1.61298 hours remaining, 114672960.000000 MB of memory in use.
ps-9389762.log:TIMING: 50000  CPU: 3208.91, 0.0800102/step  Wall: 3267.76, 0.0814126/step, 1.35688 hours remaining, 114672960.000000 MB of memory in use.
ps-9389762.log:TIMING: 60000  CPU: 4007.45, 0.0798534/step  Wall: 4081.35, 0.0813589/step, 1.12999 hours remaining, 114749888.000000 MB of memory in use.
ps-9389762.log:TIMING: 70000  CPU: 4799.9, 0.079245/step  Wall: 4888.07, 0.0806714/step, 0.896349 hours remaining, 114749888.000000 MB of memory in use.
ps-9389762.log:TIMING: 80000  CPU: 5591.32, 0.0791418/step  Wall: 5694.24, 0.0806172/step, 0.67181 hours remaining, 114749888.000000 MB of memory in use.
ps-9389762.log:TIMING: 90000  CPU: 6386.25, 0.0794938/step  Wall: 6502.93, 0.0808687/step, 0.44927 hours remaining, 114749888.000000 MB of memory in use.
ps-9389762.log:TIMING: 100000  CPU: 7182.17, 0.0795919/step  Wall: 7314.37, 0.0811448/step, 0.225402 hours remaining, 114749888.000000 MB of memory in use.
ps-9389762.log:TIMING: 110000  CPU: 7975.87, 0.0793691/step  Wall: 8123.41, 0.0809032/step, 0 hours remaining, 114749888.000000 MB of memory in use.
ps-9389781.log:Charm++> Running in SMP mode: 1 processes, 31 worker threads (PEs) + 1 comm threads per process, 31 PEs total
ps-9389781.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9389781.log:Pe 24 physical rank 24 binding to CUDA device 3 on g13r2n07: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:63:0
ps-9389781.log:Pe 8 physical rank 8 binding to CUDA device 1 on g13r2n07: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:26:0
ps-9389781.log:Pe 16 physical rank 16 binding to CUDA device 2 on g13r2n07: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:43:0
ps-9389781.log:Pe 4 physical rank 4 binding to CUDA device 0 on g13r2n07: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9389781.log:Info: Running on 31 processors, 1 nodes, 1 physical nodes.
ps-9389781.log:Info: TIMING OUTPUT STEPS    10000
ps-9389781.log:TCL: Running for 100000 steps
ps-9389781.log:TIMING: 20000  CPU: 794.739, 0.079291/step  Wall: 849.206, 0.0847033/step, 2.11758 hours remaining, 116644176.000000 MB of memory in use.
ps-9389781.log:TIMING: 30000  CPU: 1602.27, 0.0807528/step  Wall: 1713.18, 0.086397/step, 1.91993 hours remaining, 116737056.000000 MB of memory in use.
ps-9389781.log:TIMING: 40000  CPU: 2412.24, 0.0809974/step  Wall: 2572.24, 0.0859067/step, 1.67041 hours remaining, 116737056.000000 MB of memory in use.
ps-9389781.log:TIMING: 50000  CPU: 3216.36, 0.0804122/step  Wall: 3424.35, 0.0852111/step, 1.42019 hours remaining, 116737056.000000 MB of memory in use.
ps-9389781.log:TIMING: 60000  CPU: 4014.52, 0.0798157/step  Wall: 4272.2, 0.0847847/step, 1.17756 hours remaining, 116813984.000000 MB of memory in use.
ps-9389781.log:TIMING: 70000  CPU: 4819.11, 0.080459/step  Wall: 5120.87, 0.0848665/step, 0.942962 hours remaining, 116813984.000000 MB of memory in use.
ps-9389781.log:TIMING: 80000  CPU: 5625.57, 0.0806462/step  Wall: 5970.91, 0.0850045/step, 0.708371 hours remaining, 116813984.000000 MB of memory in use.
ps-9389781.log:TIMING: 90000  CPU: 6430.92, 0.0805346/step  Wall: 6819.68, 0.0848772/step, 0.47154 hours remaining, 116813984.000000 MB of memory in use.
ps-9389781.log:TIMING: 100000  CPU: 7229.23, 0.0798306/step  Wall: 7666.63, 0.084695/step, 0.235264 hours remaining, 116813984.000000 MB of memory in use.
ps-9389781.log:TIMING: 110000  CPU: 8032.27, 0.080305/step  Wall: 8512.9, 0.0846271/step, 0 hours remaining, 116813984.000000 MB of memory in use.

ps-9401393.log:Charm++> Running in SMP mode: 4 processes, 6 worker threads (PEs) + 1 comm threads per process, 24 PEs total
ps-9401393.log:Charm++> Running on 1 hosts (1 sockets x 32 cores x 1 PUs = 32-way SMP)
ps-9401393.log:Pe 18 physical rank 18 binding to CUDA device 0 on g11r1n01: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:63:0
ps-9401393.log:Pe 6 physical rank 6 binding to CUDA device 0 on g11r1n01: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:26:0
ps-9401393.log:Pe 12 physical rank 12 binding to CUDA device 0 on g11r1n01: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:43:0
ps-9401393.log:Pe 4 physical rank 4 binding to CUDA device 0 on g11r1n01: 'Device 66a1'  Mem: 16368MB  Rev: 7.5  PCI: 0:4:0
ps-9401393.log:Info: Running on 24 processors, 4 nodes, 1 physical nodes.
ps-9401393.log:Info: TIMING OUTPUT STEPS    10000
ps-9401393.log:TCL: Running for 100000 steps
ps-9401393.log:TIMING: 20000  CPU: 913.952, 0.0912708/step  Wall: 929.919, 0.0928546/step, 2.32136 hours remaining, 90175600.000000 MB of memory in use.
ps-9401393.log:TIMING: 30000  CPU: 1825.51, 0.0911563/step  Wall: 1857.72, 0.0927798/step, 2.06177 hours remaining, 90229744.000000 MB of memory in use.
ps-9401393.log:TIMING: 40000  CPU: 2735.21, 0.09097/step  Wall: 2781.51, 0.0923793/step, 1.79626 hours remaining, 90231696.000000 MB of memory in use.
ps-9401393.log:TIMING: 50000  CPU: 3648.78, 0.0913567/step  Wall: 3710.28, 0.0928771/step, 1.54795 hours remaining, 90231696.000000 MB of memory in use.
ps-9401393.log:TIMING: 60000  CPU: 4558.98, 0.0910195/step  Wall: 4634.17, 0.0923889/step, 1.28318 hours remaining, 90261600.000000 MB of memory in use.
ps-9401393.log:TIMING: 70000  CPU: 5473.67, 0.091469/step  Wall: 5564.06, 0.0929893/step, 1.03321 hours remaining, 90299104.000000 MB of memory in use.
ps-9401393.log:TIMING: 80000  CPU: 6384.6, 0.0910938/step  Wall: 6490.17, 0.0926112/step, 0.77176 hours remaining, 90299104.000000 MB of memory in use.
ps-9401393.log:TIMING: 90000  CPU: 7294.78, 0.0910176/step  Wall: 7415.33, 0.0925151/step, 0.513973 hours remaining, 90326880.000000 MB of memory in use.
ps-9401393.log:TIMING: 100000  CPU: 8205.93, 0.0911148/step  Wall: 8342.06, 0.0926739/step, 0.257428 hours remaining, 90589024.000000 MB of memory in use.
```

注意，有几个log文件是运行中的部分结果，分析使用的s/step指标是最后一次输出的性能数值，可以看出，每个log文件中该指标基本稳定。

